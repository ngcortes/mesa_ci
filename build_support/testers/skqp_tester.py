try:
    import ConfigParser
except:
    import configparser as ConfigParser
import os
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et
from dependency_graph import DependencyGraph
from export import Export
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet
from utils.command import run_batch_command
from utils.utils import (get_conf_file, get_libdir, get_libgl_drivers,
                         NoConfigFile, mesa_version, cpu_count, get_blacklists)


def process_test_result(result_elementtree, config, missing_revisions):
    """ When given a test result elementtree, return an updated elementtree
    to indicate test is skipped if the given config indicates that it
    should be. """
    expected_status = {}
    changed_commit = {}
    new_et = result_elementtree
    c = ConfigParser.SafeConfigParser(allow_no_value=True)
    c.read(config)
    for section in c.sections():
        for (test, commit) in c.items(section):
            if test in expected_status:
                raise Exception("test has multiple entries: " + test)
            expected_status[test] = section
            changed_commit[test] = commit
    for atest in new_et.findall(".//testcase"):
        test_name = (atest.attrib["classname"].lower() + '.'
                     + atest.attrib["name"].lower())
        # remove suffix
        test_name = '.'.join(test_name.split('.')[:-1])
        if atest.attrib["status"] == "lost":
            atest.attrib["status"] = "crash"
        if test_name not in expected_status:
            continue

        expected = expected_status[test_name]
        test_is_stale = False
        for missing_commit in missing_revisions:
            if missing_commit in changed_commit[test_name]:
                test_is_stale = True
                # change stale test status to skip
                for ftag in atest.findall("failure"):
                    atest.remove(ftag)
                for ftag in atest.findall("error"):
                    atest.remove(ftag)
                atest.append(et.Element("skipped"))
                so = et.Element("system-out")
                so.text = "WARN: the results of this were changed by " + changed_commit[test_name]
                so.text += ", which is missing from this build."
                atest.append(so)
                break
        if test_is_stale:
            continue
        if expected == "expected-failures":
            # change fail to pass
            if atest.attrib["status"] == "fail":
                for ftag in atest.findall("failure"):
                    atest.remove(ftag)
                so = et.Element("system-out")
                so.text = "Passing test as an expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "crash":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test crashed when it expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "pass":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test passed when it expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "skip":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test skipped when it expected failure"
                atest.append(so)
            else:
                raise Exception("test has unknown status: " + atest.attrib["name"]
                                + " " + atest.attrib["status"])
        elif expected == "expected-crashes":
            # change error to pass
            if atest.attrib["status"] == "crash":
                for ftag in atest.findall("error"):
                    atest.remove(ftag)
                so = et.Element("system-out")
                so.text = "Passing test as an expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "fail":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test failed when it expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "pass":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test passed when it expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "skip":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test skipped when it expected crash"
                atest.append(so)
            else:
                raise Exception("test has unknown status: " + atest.attrib["name"]
                                + " " + atest.attrib["status"])
    return new_et


class SkqpLister():
    def __init__(self, env=None):
        self.env = {}
        if env is not None:
            self.env = env
        self._tests = []
        skqp_dir = ProjectMap().build_root() + '/opt/skqp'
        out, err = run_batch_command([skqp_dir + '/skqp', '--gtest_list_tests',
                                      skqp_dir,
                                      ProjectMap().build_root() + '/tests'],
                                     streamedOutput=False, quiet=True,
                                     env=self.env)
        subgroup = None
        for test in out.decode().split('\n'):
            if not test:
                continue
            if test.endswith('.'):
                subgroup = test
                continue
            if subgroup:
                test = test.lstrip()
                if test not in self._tests:
                    self._tests.append(subgroup + test)

    def filter(self, blacklist):
        if str == type(blacklist):
            blacklist = [blacklist]
        if isinstance(blacklist, type(SkqpLister)):
            blacklist = blacklist._tests
        for test in blacklist:
            if not test:
                continue
            if test.startswith("#"):
                continue
            if test in self._tests:
                self._tests.remove(test)

    def whitelist(self, whitelist):
        for test in self._tests:
            if test not in whitelist:
                self._tests.remove(test)

    def add(self, testlist):
        self._tests.extend(testlist._tests)

    def list(self):
        return self._tests

    def getone(self):
        return self._tests.pop()

    def count(self):
        return len(self._tests)

    def remove(self, test):
        if test in self._tests:
            self._tests.remove(test)


class SkqpTester():

    def __init__(self, env=None):
        self._options = Options()
        self._pm = ProjectMap()
        self.tests = []  # List of tests to run
        self.gtests = ['skqp']
        self.env = {}
        if env is not None:
            self.env = env

        self.project = self._pm.current_project()

        self._src_dir = self._pm.project_source_dir(self.project)
        self._build_root = self._pm.build_root()
        deps = DependencyGraph(self.project,
                               Options(args=[sys.argv[0]]),
                               repo_set=None)
        long_revisions = RepoSet().branch_missing_revisions(deps)
        self.missing_revisions = [a_rev[:6] for a_rev in long_revisions]

    def test(self):
        # SKQP does not run reliably on anything older than Mesa 19.0
        if '18.' in mesa_version():
            return
        self.env.update(os.environ)
        self.env.update({
            'LD_LIBRARY_PATH': ':'.join([
                get_libdir(),
                os.path.join(get_libdir(), 'dri'),
            ]),
            "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
        })
        results_dir = os.path.abspath(os.path.join(
            self._pm.build_root(), '../test'))
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
        self.tests = SkqpLister(env=self.env)
        gtest_path = self._build_root + '/opt/skqp'
        try:
            conf_file = get_conf_file(self._options.hardware,
                                      self._options.arch,
                                      project=self.project)
        except NoConfigFile:
            print(('No config file found for hardware: {0} '
                   'arch: {1}').format(self._options.hardware,
                                       self._options.arch),
                  file=sys.stderr)
            sys.exit(1)

        for blacklist in get_blacklists():
            self.tests.filter([l.rstrip() for l in open(blacklist)])

        root = et.Element('testsuites')
        suite = et.SubElement(
            root,
            'testsuite',
            name=self._pm.current_project(),
            tests=str(len(self.tests.list())))

        filename = os.path.join(
            results_dir, 'piglit-' + self._pm.current_project()
            + '_'.join(['-unittest', self._options.config, self._options.arch,
                        self._options.hardware]) + '.xml')
        proc_env = self.env
        # At least one skqp test performs shader compiles (GLPrograms)
        proc_env['MESA_GLSL_CACHE_DISABLE'] = 'true'
        completion_percentage = 0
        completed_tests = 0
        prev_completed_tests = 0
        sleep_time_ms = 0.0
        cpus = cpu_count()
        procs = dict.fromkeys(range(1, cpus + 1))
        remaining_test_count = self.tests.count()
        full_test_count = remaining_test_count
        # note: gtest_filter and gtest_ouput=xml are set later on
        cmd = [gtest_path + '/skqp',
               '--gtest_output=xml:',
               '--gtest_filter=', gtest_path,
               results_dir]
        while (full_test_count - completed_tests) > 0:
            for cpu in range(1, cpus + 1):
                if procs[cpu] is None:
                    try:
                        # This test generally takes longer than the rest of
                        # the tests combined, so start it first
                        if 'Skia_Unit_Tests.GLPrograms' in self.tests.list():
                            self.tests.remove('Skia_Unit_Tests.GLPrograms')
                            core_test = 'Skia_Unit_Tests.GLPrograms'
                        else:
                            core_test = self.tests.getone()
                    except IndexError:
                        # no more tests to add
                        break
                else:
                    # core already has a test
                    continue
                test_result_xml = '_'.join(['gtest', core_test,
                                            self._options.config,
                                            self._options.arch,
                                            self._options.hardware]) + '.xml'
                test_result_xml = os.path.join(results_dir, test_result_xml)
                # splice in test name and results file into the command list
                core_cmd = [cmd[0], cmd[1] + test_result_xml,
                            cmd[2] + core_test] + cmd[3:]
                err_fh = tempfile.TemporaryFile('w+')
                out_fh = tempfile.TemporaryFile('w+')
                proc = subprocess.Popen(core_cmd, env=proc_env, stdout=out_fh,
                                        stderr=err_fh)
                proc.test_result_xml = test_result_xml
                proc.test_name = core_test
                proc.out_fh = out_fh
                proc.err_fh = err_fh
                procs[cpu] = proc
            # Print progress
            new_percentage = (completed_tests * 100) / full_test_count
            if new_percentage > completion_percentage:
                completion_percentage = new_percentage
                print("[ " + str(round(completion_percentage, 2)) + "% ]")
            # No tests completed when we polled.  Delay to
            # avoid a spin loop.
            # This uses a backoff timer since some tests are very fast but
            # some may take a few seconds or more to complete
            if prev_completed_tests == completed_tests:
                time.sleep(sleep_time_ms / 1000.0)
                if sleep_time_ms < 10.0:
                    sleep_time_ms = 10.0
                elif sleep_time_ms <= 5000:
                    sleep_time_ms *= 2.0
            else:
                prev_completed_tests = completed_tests
                sleep_time_ms = 0.0
            # Process completed test(s)
            for cpu, proc in procs.items():
                if proc is None:
                    continue
                proc.poll()
                if proc.returncode is None:
                    continue
                test_result_xml = proc.test_result_xml

                proc.err_fh.seek(0)
                proc.out_fh.seek(0)
                try:
                    ts_xml = et.parse(test_result_xml)
                    # skqp does not set status tag to anything useful for us,
                    # so try to determine if this was a pass or fail and
                    # set the status tag accordingly so filtering works
                    for testcase in ts_xml.findall('.//testcase'):
                        if testcase.find('.//failure') is None:
                            testcase.set('status', 'pass')
                        else:
                            testcase.set('status', 'fail')
                            system_err = et.SubElement(testcase, 'system-err')
                            system_err.text = ''.join(proc.err_fh.readlines())
                        # Add platform/arch suffix to test
                        name = (testcase.get('name') + '.'
                                + self._options.hardware + self._options.arch)
                        testcase.set('name', name)
                        system_out = et.SubElement(testcase, 'system-out')
                        system_out.text = ''.join(proc.out_fh.readlines())
                except (IOError, et.ParseError):
                    ts_xml = Export().create_failing_test_xml("failing-gtest-"
                                                              + proc.test_name,
                                                              "ERROR: gtest "
                                                              "crashed "
                                                              + proc.test_name)
                    testcase = ts_xml.find('.//testcase')
                    system_err = et.SubElement(testcase, 'system-err')
                    system_err.text = ''.join(proc.err_fh.readlines())
                    system_out = et.SubElement(testcase, 'system-out')
                    system_out.text = ''.join(proc.out_fh.readlines())

                proc.err_fh.close()
                proc.out_fh.close()
                ts_xml = process_test_result(ts_xml, conf_file,
                                             self.missing_revisions)
                for testcase in ts_xml.findall('.//testcase'):
                    suite.append(testcase)
                procs[cpu] = None
                completed_tests += 1
                # test results are summarized into one xml at the end, so
                # individual test xml is no longer needed
                if os.path.exists(test_result_xml):
                    os.remove(test_result_xml)
        # save all results
        tree = et.ElementTree(root)
        tree.write(filename, encoding='utf-8', xml_declaration=True)

        Export().export_tests()

    def build(self):
        pass

    def clean(self):
        pass
