import os
import shutil
import socket
import subprocess
import sys
import time
from utils.check_gpu_hang import check_gpu_hang
from utils.command import rmtree, run_batch_command
from export import Export, fail_and_reboot
from utils.utils import (DefaultTimeout, NullInvoke, get_libdir,
                         get_libgl_drivers, mesa_version)
from utils.timer import TimeOut
from options import Options
from project_invoke import ProjectInvoke
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None

class NullTimeout:
    def __init__(self):
        pass
    def start(self):
        return
    def end(self):
        return
    def GetDuration(self):
        return 100000

def build(builder, options=None, time_limit=None, import_build=True):

    if not time_limit:
        time_limit = DefaultTimeout()
    if not options:
        options = Options()
    action_map = [
        ("clean", builder.clean),
        ("build", builder.build),
        ("test", builder.test),
    ]
    actions = options.action

    invoke = NullInvoke()

    if "PKG_CONFIG_PATH" in os.environ:
        del os.environ["PKG_CONFIG_PATH"]
    if "LD_LIBRARY_PATH" in os.environ:
        del os.environ["LD_LIBRARY_PATH"]
    if "LIBGL_DRIVERS_PATH" in os.environ:
        del os.environ["LIBGL_DRIVERS_PATH"]

    # TODO: add this stuff
    if (options.result_path):
        # if we aren't posting to a server, don't attempt to write
        # status
        invoke = ProjectInvoke(options)

    invoke.set_info("start_time", time.time())
    if "NODE_NAME" in os.environ:
        invoke.set_info("machine", os.environ["NODE_NAME"])

    if options.hardware != "builder" and check_gpu_hang():
        raise RuntimeError("ERROR: GPU hang detected, not running tests!")

    # start a thread to limit the run-time of the build
    to = TimeOut(time_limit, invoke=invoke)
    to.start()

    exporter = Export()
    if import_build:
        exporter.import_build_root()

    if type(actions) is str:
        actions = [actions]

    # clean out the test results directory, so jenkins processes only
    # the files for the current build
    if "test" in actions:
        test_out_dir = ProjectMap().source_root() + "/test"
        if os.path.exists(test_out_dir):
            rmtree(test_out_dir)
        br_test_out_dir = ProjectMap().build_root() + "/../test"
        if os.path.exists(br_test_out_dir):
            rmtree(br_test_out_dir)

    # Walk through the possible actions in order, if those actions are not
    # requested go on. The order does matter.

    for k, a in action_map:
        if k not in actions:
            continue
        options.action = a

        # Mapping of platform hardware type to string used for searching
        # i965_pci_ids.h for support
        sim_pciids_name = {
            'tgl_sim': 'TGL',
        }
        if internal_vars:
            sim_pciids_name.update(internal_vars.sim_pciids_name)

        # Note: this check for supported gpu should be skipped when 'building'
        # fulsim since it doesn't depend on the 'mesa' project and will almost
        # certainly fail
        if (k == "test" and options.hardware != "builder"
                and ProjectMap().current_project() != "fulsim"):
            hostname = socket.gethostname()
            # Make sure that simulated platforms are supported in the mesa
            # being tested
            if '_sim' in options.hardware:
                pciid_name = None
                if options.hardware in sim_pciids_name:
                    pciid_name = sim_pciids_name[options.hardware]
                try:
                    if not pciid_name:
                        raise
                    file_names = ['i965_pci_ids.h', 'iris_pci_ids.h']
                    mesa_pciids_dir = (ProjectMap().source_root()
                                       + '/repos/mesa/include/pci_ids/')

                    if not os.path.exists(mesa_pciids_dir):
                        raise
                    found = False
                    for file_name in file_names:
                        mesa_pciids_file = mesa_pciids_dir + file_name
                        try:
                            with open(mesa_pciids_file, 'r') as f:
                                for line in f.readlines():
                                    if pciid_name in line:
                                        found = True
                                        break
                        except FileNotFoundError:
                            continue
                        finally:
                            if not found:
                                print('pciid not found, trying another file')

                        if found:
                            break

                    if not found:
                        raise
                except Exception:
                    Export().create_failing_test("unsupported-gpu-pciid-"
                                                 + hostname,
                                                 "This gpu is not supported "
                                                 + "in the Mesa under test.")
                    to.end()
                    sys.exit("This gpu is not supported in the Mesa under "
                             + "test.")
            else:
                br = ProjectMap().build_root()
                env = {}
                env.update({
                    'LD_LIBRARY_PATH': ':'.join([
                        get_libdir(),
                        get_libgl_drivers(),
                    ]),
                    "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
                })
                if "iris" in options.hardware:
                    env["MESA_LOADER_DRIVER_OVERRIDE"] = "iris"
                elif "crocus" in options.hardware:
                    env["MESA_LOADER_DRIVER_OVERRIDE"] = "crocus"
                options.update_env(env)

                # make sure not using llvm pipe
                wflinfo = br + "/bin/wflinfo"

                # Start X server if instructed, but only if the action is
                # 'test' and hardware is not builder (this condition is further
                # up)
                if options.startx:
                    # stop any existing X process
                    cmd = ['killall', 'Xorg']
                    run_batch_command(cmd, ignore_fail=True)
                    time.sleep(5)
                    # start X server
                    cmd = ['Xorg', ':0', 'vt0', '-retro', '-sharevts',
                           '-noreset']
                    run_batch_command(cmd, background=True, env=env)
                    sleep_time = 2
                    while sleep_time < 33:
                        # check that X started successfully
                        time.sleep(sleep_time)
                        try:
                            run_batch_command([wflinfo,
                                               "--platform=glx",
                                               "-a", "gl"],
                                              streamedOutput=False,
                                              env=env)
                            break
                        except subprocess.CalledProcessError:
                            # X not launched yet
                            sleep_time = sleep_time * 2

                    # disable X power management
                    cmd = ['xset', 'dpms', 'force', 'on']
                    run_batch_command(cmd, env=env, ignore_fail=True)
                    cmd = ['xset', 's', 'off']
                    run_batch_command(cmd, env=env, ignore_fail=True)

                check_platform = ""
                if "dg2" in options.hardware:
                    check_platform = "DG2"
                windowing_platforms = ['gbm']
                if options.startx:
                    windowing_platforms.append('glx')
                for platform in windowing_platforms:
                    try:
                        print("Checking for support for " + platform)
                        (out, _) = run_batch_command([wflinfo,
                                                      f"--platform={platform}",
                                                      "-a", "gl"],
                                                     streamedOutput=False,
                                                     env=env)
                        if "renderer string: llvmpipe" in out.decode():
                            invoke.set_info("end_time", time.time())
                            invoke.set_info("status", "failed")

                            fail_out = out.decode()

                            Export().create_failing_test("llvmpipe-detected-"
                                                         + hostname,
                                                         fail_out)
                            print("ERROR: build aborted")
                            to.end()
                            sys.exit("llvmpipe software rendering detected. "
                                     "Aborting test.")
                        if check_platform not in out.decode():
                            print(f"ERROR: mesa is not testing the desired hardware platform: {check_platform} on {platform}")
                            Export().create_failing_test(f"wrong-card-{check_platform}-{hostname}-{platform}",
                                                         out.decode())
                            to.end()
                            sys.exit("wrong card rendering detected. Aborting test.")
                            
                    except AssertionError as e:
                        print(e)
                        print("WARN: skipping llvmpipe check.")
                    except subprocess.CalledProcessError as e:
                        print(e)
                        print("ERROR: wflinfo failed")
                        fail_and_reboot(f"wflinfo-crash-{hostname}", "wflinfo crashed!")
                        if platform != 'glx':
                            to.end()
                            sys.exit(1)
                        print("WARN: Continuing testing without X/glx "
                              "support, results for some test suites may "
                              "be incorrect")

        if k == "test" and "CACHE_DISABLE" in options.env:
            shader_cache_test = True
        else:
            shader_cache_test = False
        try:
            if shader_cache_test:
                print("Deleting shader cache")
                cache_dir = os.path.expanduser("~/.cache/mesa_shader_cache")
                if os.path.exists(cache_dir):
                    shutil.rmtree(cache_dir)
            a()
            if shader_cache_test:
                print("Running a second time to test shader cache.")
                a()
        except:
            # we need to cancel the timer first, in case
            # set_status fails, and the timer is left running
            to.end()
            invoke.set_info("status", "failed")
            # must cancel timeout timer, which will prevent process from ending
            raise

    if import_build:
        # tidy up the temporary build root that was used to identify
        # new files
        exporter.clean_orig_build_root()

    # must cancel timeout timer, which will prevent process from
    # ending.  cancel the timer first, in case set_status fails, and
    # the timer is left running
    to.end()
    print("build finished")
    invoke.set_info("end_time", time.time())
    invoke.set_info("status", "success")
