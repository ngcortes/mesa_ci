# Copyright (C) Intel Corp.  2014-2019.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/

import sys, os
import xml.etree.cElementTree as et
import pdb

from options import Options

from .build_spec import BuildSpec

class ProjectMap:
    """provides convenient and consistent access to paths which are
    necessary to the builds"""

    def __init__(self):
        """locate the build specification document, to use as a reference
        point for all other paths"""
        root = os.path.dirname(os.path.abspath(sys.argv[0]))
        if "py.test" in sys.argv[0]:
            root = os.getcwd()
        while True:
            build_spec = root + "/build_specification.xml"
            if not os.path.exists(build_spec):
                if (os.path.dirname(root) == root):
                    # we are at "/"
                    assert(False)
                    return

                root = os.path.dirname(root)
                continue

            # else we have found the spec
            self._source_root = root
            break
        self._internal_source_root = None
        if os.path.exists(os.path.join(self._source_root, root,
                                       "repos/mesa_ci_internal")):
            self._internal_source_root = os.path.join(self._source_root, root,
                                                      "repos/mesa_ci_internal")
        # cache the current_project, because it can't be recalculated
        # if the caller changes directories.
        self._current_project = None
        self._current_project = self.current_project()
        self._merged_build_spec = None

    def source_root(self):
        """top directory, which contains the build_specification.xml"""
        return self._source_root

    def internal_source_root(self):
        """directory which contains the internal build_specification.xml"""
        return self._internal_source_root

    def build_root(self):
        """chroot directory where all results are placed during a build"""
        br = "/tmp/build_root/" + Options().arch
        if not os.path.exists(br):
            os.makedirs(br)
        return br

    def test_dir(self):
        td = "/tmp/build_root/test"
        if not os.path.exists(td):
            os.makedirs(td)
        return td

    def project_build_dir(self, project=None):
        """location of the build.py for the project"""
        if project is None:
            project = self._current_project
        cb = self._source_root + "/" + project + "/"
        return cb

    def project_source_dir(self, project=None):
        """location of the git repo for the project"""
        if project is None:
            project = self.current_project()
        spec = self.build_spec()
        repo_name = spec.repo_name_for_project(project)
        if not repo_name:
            return None
        sdir = self._source_root + "/repos/" + repo_name
        if not os.path.exists(sdir):
            return None
        return sdir

    def current_project(self):
        """name of the project which is invoking this method"""
        if self._current_project:
            return self._current_project
        build_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        return os.path.split(build_dir)[1]

    def output_dir(self):
        """logs / test xml go in this directory"""
        o = Options()
        if o.result_path:
            return os.path.abspath(o.result_path + "/test")
        return self._source_root + "/results"

    def _merge_build_specs(self, build_specs):
        """ merge any number of xml trees listed in build_specs into the first
            tree in the list and returns the first tree in the list """
        xml_roots = [xml.getroot() for xml in build_specs]
        # loop through and merge subsequent roots with the first root
        for root in xml_roots[1:]:
            self._merge_projects(xml_roots[0], root)
            self._merge_branches(xml_roots[0], root)
            
        return build_specs[0]

    def _merge_projects(self, dest, src):
        dest_projects = dest.find("projects")
        src_projects = src.find("projects")
        for project in src_projects:
            src_name = project.attrib["name"]
            dest_project = dest_projects.find("project[@name='" + src_name + "']")
            if not dest_project:
                dest_projects.append(project)
                continue
            self._merge_project(dest_project, project)

    def _merge_project(self, dest, src):
        assert(dest.attrib["name"] == src.attrib["name"])
        for k,v in src.attrib.items():
            if k not in dest.attrib:
                dest.attrib[k] = v
                continue
            src_values = v.split(',')
            dest_values = dest.attrib[k].split(',')
            for s_v in src_values:
                if s_v not in dest_values:
                    dest_values.append(s_v)
            dest.attrib[k] = ",".join(dest_values)
        for src_prereq in src:
            assert(src_prereq.tag == "prerequisite")
            dest.append(src_prereq)

    def _merge_branches(self, dest, src):
        dest_branches = dest.find("branches")
        src_branches = src.find("branches")
        if not src_branches:
            return
        for branch in src_branches.findall("branch"):
            dest_branches.append(branch)
            
    def build_spec(self):
        if not self._merged_build_spec:
            external_bs = et.parse(self.source_root()
                                   + "/build_specification.xml")
            if (not self.internal_source_root()
                    or not os.path.exists(self.internal_source_root()
                                          + "/build_specification.xml")):
                self._merged_build_spec = external_bs
                return BuildSpec(self._merged_build_spec)
            internal_bs = et.parse(self.internal_source_root()
                                   + "/build_specification.xml")
            self._merged_build_spec = self._merge_build_specs([external_bs,
                                                              internal_bs])
        return BuildSpec(self._merged_build_spec)

    def string_spec(self):
        return self.build_spec().string_spec()
