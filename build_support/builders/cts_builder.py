import glob
import os

from testers.deqp_tester import deqp_external_revisions
from builders import CMakeBuilder
from utils.command import run_batch_command
from export import Export
from utils.utils import git_clean, get_package_config_path, cpu_count
from options import Options
from project_map import ProjectMap
from repo_set import checkout_externals


# mostly this is necessary because cts has no make install
class CtsBuilder(CMakeBuilder):
    def __init__(self, suite):
        self._suite = suite
        assert(suite == "gl" or suite == "es")
        arch = Options().arch
        extra_definitions=["-DCMAKE_INCLUDE_PATH=/tmp/build_root/" + arch + "/include",
                           "-DCMAKE_LIBRARY_PATH=/tmp/build_root/" + arch + "/lib"]
        if suite == "gl":
            extra_definitions.append("-DDEQP_TARGET=x11_egl")
        else:
            extra_definitions += ["-DDEQP_TARGET=x11_egl",
                                  "-DDEQP_GLES1_LIBRARIES=/tmp/build_root/"]
        CMakeBuilder.__init__(self, extra_definitions=extra_definitions)
            
    def test(self):
        pass

    def clean(self):
        git_clean(self._src_dir)

    def build(self):
        pm = ProjectMap()
        project = pm.current_project()
        revs = deqp_external_revisions(project)
        external_dir = (pm.project_source_dir(project)
                        + "/external/{}/src")
        checkout_externals(project, revisions=revs,
                           external_dir_format=external_dir)
        git_env = {"GIT_AUTHOR_NAME" : "Mesa CI",
                   "GIT_AUTHOR_EMAIL" : "mesa_ci@intel.com",
                   "GIT_COMMITTER_NAME" : "Mesa CI",
                   "GIT_COMMITTER_EMAIL" : "mesa_ci@intel.com",
                   "EMAIL" : "mesa_ci@intel.com" }
        # apply patches if they exist
        for patch in sorted(glob.glob(pm.project_build_dir() + "/*.patch")):
            os.chdir(self._src_dir)
            try:
                run_batch_command(["git", "am", patch],
                                  env = git_env)
            except:
                print("WARN: failed to apply patch: " + patch)
                run_batch_command(["git", "am", "--abort"],
                                  env = git_env)

        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)

        savedir = os.getcwd()
        os.chdir(self._build_dir)

        cflag = "-m32"
        cxxflag = "-m32"
        if self._options.arch == "m64":
            cflag = "-m64"
            cxxflag = "-m64"
        env = {"CC":"ccache gcc",
               "CXX":"ccache g++",
               "CFLAGS":cflag,
               "CXXFLAGS":cxxflag,
               "PKG_CONFIG_PATH":get_package_config_path()}
        self._options.update_env(env)
        kc_cts_target = ""
        if self._suite == "gl":
            kc_cts_target = "-DGLCTS_GTF_TARGET=gl"

        run_batch_command(["cmake", "-GNinja", kc_cts_target, self._src_dir] + self._extra_definitions,
                             env=env)

        run_batch_command(["ninja","-j" + str(cpu_count())], env=env)
        install_dir = pm.build_root() + "/bin/" + self._suite
        binary_dir = self._build_dir + "/external/openglcts/modules"
        run_batch_command(["mkdir", "-p", install_dir])
        run_batch_command(["cp", "-a", binary_dir,
                              install_dir])

        run_batch_command(["cp", "-a", self._build_dir + "/external/openglcts/modules/gl_cts/data/mustpass/gles/khronos_mustpass",
                              pm.build_root() + "/share"])

        os.chdir(savedir)

        Export().export()
