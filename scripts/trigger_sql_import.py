import sys
import os

from urllib.parse import urlparse as parse
from urllib.request import urlopen, quote

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))

from utils.utils import reliable_url_open

url_components = parse(os.environ["BUILD_URL"])

export_url = ("http://" + url_components.netloc +
              "/job/public/job/export_public_results/buildWithParameters?"
              "url={0}".format(quote(os.environ["BUILD_URL"])))

print("triggering: " + export_url)
reliable_url_open(export_url, user="mesa-ci", method="POST")
