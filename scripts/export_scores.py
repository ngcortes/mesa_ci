#!/usr/bin/env python3
import argparse
import ast
import git
import glob
import hashlib
import json
import os
import pwd
import sys
import subprocess
import tarfile
import tempfile
import xml.etree.ElementTree as et
from urllib.request import urlopen
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from project_map import ProjectMap
from repo_set import RevisionSpecification
from utils.command import rmtree


hardware_whitelist = ['bdw', 'bsw', 'builder', 'bxt', 'byt', 'cfl', 'g33',
                      'g45', 'g965', 'glk', 'glk_iris', 'hsw', 'ilk', 'ivb',
                      'kbl', 'kbl_iris', 'skl', 'snb',
                      'gen9', 'gen9atom', 'gen9_iris', 'gen9atom_iris',
                      'bdw_iris', 'icl', 'icl_iris']

project_map = {"mesa_master_daily": "mesa_master",
               "vulkancts_daily": "vulkancts"}

job_map = {"perf":  "mesa_master",
           "perf_random": "mesa_master"
           }


def parse_revisions(rev_table):
    # first row is the column headers
    # needed to discover build path if component build not available
    revisions = {}
    columns = []
    for column in rev_table[0].findall('td'):
        columns.append(column.attrib["value"])
    for rev in rev_table[1:]:
        index = 0
        project_details = {}
        for column in rev.findall('td'):
            project_details[columns[index]] = column.attrib["value"]
            index = index + 1
        (project, sha) = project_details["commit"].split("=")
        project_details["sha"] = sha
        revisions[project] = project_details
    return revisions


def create_build_tar(url, use_hardware_whitelist=True):
    project = url.split("/")[4]
    if project in project_map:
        # fix jobs that do not have the same name as the project
        project = project_map[project]

    try:
        f = urlopen(url + "/api/python")
        build_page = ast.literal_eval(f.read().decode("utf-8"))
        f = urlopen(url + "/artifact/summary.xml")
        summary_table = et.fromstring(f.read())
    except Exception as e:
        print("Could not export: " + url)
        print(e)
        return None

    tables = summary_table.findall('table')
    revisions = parse_revisions(tables[0].findall('tr'))
    proj_revs = {}
    for proj, rev in revisions.items():
        proj_revs[proj] = rev['sha']

    try:
        revspec = RevisionSpecification(revisions=proj_revs)
        hashstr = hashlib.md5(revspec.to_cmd_line_param().encode()).hexdigest()
    except Exception as e:
        print("Could not export: " + url)
        print(e)
        return
    results_dir = "/mnt/jenkins/results"
    job_name = url.split("/")[4]
    if job_name in job_map:
        job_name = job_map[job_name]

    result_path = "/".join([results_dir, job_name, hashstr, "developer"])
    if not os.path.exists(result_path):
        print("Unable to find results in path: " + result_path)
        result_path = "/".join([results_dir, job_name, 'mesa=' +
                                proj_revs['mesa'], "developer"])

    if not os.path.exists(result_path):
        print("ERROR: Could not find any results.")
        return

    print("Found results in path: " + result_path)

    # collect list of score json files to send
    score_globs = []
    if use_hardware_whitelist:
        for hw in hardware_whitelist:
            score_globs.append(result_path + "/m64/scores/*/"
                               + hw + "*/*.json")
    else:
        score_globs = [result_path + "/m64/scores/*/*/*.json"]

    score_files = []
    for score_glob in score_globs:
        for score_file in glob.glob(score_glob):
            score_files.append(score_file)

    # try to find branchpoint in score json files first.
    # this is preferred since the ref used to look up the branchpoint has a
    # better chance of existing if it was captured when the testers were
    # running.
    mesa_bp = None
    for score_file in score_files:
        with open(score_file) as f:
            try:
                contents = json.load(f)
            except AttributeError:
                continue
        mesa_bp = contents.get('branchpoint')
        if mesa_bp:
            break
    # this is less reliable, since the ref used to get the branchpoint may have
    # been rebased away by now
    if not mesa_bp:
        mesa_bp = revspec.get_branchpoint('mesa')
    if not mesa_bp:
        print("Unable to find a branch point in mesa_master for "
              f"{revisions['mesa']}")
    mesa_repo = git.Repo(ProjectMap().project_source_dir("mesa"))
    mesa_commit = mesa_repo.commit(proj_revs['mesa'])

    build_info = {
        "job": job_name,
        "build": build_page["id"],
        "name": build_page["displayName"],
        "start_time": float(build_page["timestamp"]) / 1000,
        "end_time": (float(build_page["timestamp"]
                           + build_page["duration"]) / 1000),
        "url": url,
        "result_path": result_path,
        "revisions": revisions,
        "mesa_date":  mesa_commit.committed_date,
        "branchpoint":  mesa_bp
        }
    tmpdir = tempfile.mkdtemp()
    build_info_file = tmpdir + "/build_info.json"
    with open(build_info_file, "w") as fh:
        fh.write(json.dumps(build_info))

    comp_filename = "results_%s.%s.%s.tar" % (build_info["name"],
                                              build_info["job"],
                                              build_info["build"])
    comp_filename = comp_filename.replace("/", "_")
    comp_filepath = "%s/%s" % (tmpdir, comp_filename)

    with tarfile.open(comp_filepath, "w") as tf:
        cwd = os.getcwd()
        os.chdir(tmpdir)
        tf.add("build_info.json")
        for score in score_files:
            tf.add(score)
        os.chdir(cwd)
    cmd = ["xz", "-T0", comp_filepath]

    ret = subprocess.call(cmd)
    comp_filepath += ".xz"
    if ret:
        raise RuntimeError("Compression with xz failed!")

    print("Results successfully compressed to: %s" % comp_filepath)
    return comp_filepath


# TODO: duplicated from export_results.py
def upload_tar(tar_file):
    scp_cmd = ['scp']
    scp_host = ""
    # Disable host key verification
    scp_cmd.extend(['-o', 'StrictHostKeyChecking=no'])
    scp_cmd.extend(['-o', 'UserKnownHostsFile=/dev/null'])
    user = pwd.getpwuid(os.getuid()).pw_name
    if "MESA_CI_RESULT_HOST_PORT" in os.environ:
        scp_port = os.environ["MESA_CI_RESULT_HOST_PORT"]
        scp_cmd.extend(['-P', scp_port])
    scp_keyfile = None
    if "MESA_CI_SSH_KEYFILE" in os.environ:
        scp_keyfile = os.path.expanduser(os.environ["MESA_CI_SSH_KEYFILE"])
        scp_cmd.extend(['-i', scp_keyfile])
    scp_dest = '/tmp/mesa_ci_results'
    if "MESA_CI_RESULT_HOST_PATH" in os.environ:
        scp_dest = os.environ["MESA_CI_RESULT_HOST_PATH"]
    if "MESA_CI_RESULT_USER" in os.environ:
        user = os.environ["MESA_CI_RESULT_USER"]
        scp_host = user + "@"
    host = 'localhost'
    if "MESA_CI_RESULT_HOST" in os.environ:
        host = os.environ["MESA_CI_RESULT_HOST"]
    scp_cmd.append(tar_file)
    scp_host += host
    scp_dest = '%s:%s' % (scp_host, scp_dest)
    scp_cmd.append(scp_dest)
    subprocess.call(scp_cmd)


def export_all_builds(url, use_hardware_whitelist):
    f = urlopen(url + "/api/python")
    job_page = ast.literal_eval(f.read().decode("utf-8"))
    for build in job_page["builds"]:
        tar_file = create_build_tar(build["url"], use_hardware_whitelist)
        if not tar_file:
            continue
        upload_tar(tar_file)
        rmtree(os.path.normpath(tar_file + "/.."))


def main():
    parser = argparse.ArgumentParser(description=("Exports performance scores "
                                                  "a remote system for "
                                                  "importing into SQL"))
    parser.add_argument('--url', type=str, required=True,
                        help="Jenkins url for the build (eg, " +
                        "http://otc-mesa-ci.jf.intel.com/job/mesa_master/13643/")
    parser.add_argument('--internal', action='store_true',
                        help=('Include hardware that is not in the hardware '
                              'whitelist'))
    opts = parser.parse_args()
    build_id = None
    url_components = opts.url.split("/")
    use_hardware_whitelist = not opts.internal
    if len(url_components) > 5:
        build_id = url_components[5]
    if build_id:
        tar_file = create_build_tar(opts.url, use_hardware_whitelist)
        if not tar_file:
            sys.exit(-1)
        upload_tar(tar_file)
        rmtree(os.path.normpath(tar_file + "/.."))
        sys.exit(0)
    else:
        # export all builds at the url
        export_all_builds(opts.url, use_hardware_whitelist)


if __name__ == "__main__":
    main()
