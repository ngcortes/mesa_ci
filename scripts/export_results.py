import argparse
import ast
import copy
import hashlib
import json
import multiprocessing
import os
import pwd
import shutil
import subprocess
import sys
import tarfile
import tempfile
import urllib
try:
    from urllib2 import urlopen, URLError, HTTPError
except:
    from urllib.request import urlopen, URLError, HTTPError
import xml.etree.ElementTree as et
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from options import Options
from project_invoke import ProjectInvoke
from repo_set import RevisionSpecification
from utils.command import run_batch_command

hardware_whitelist = ['bdw', 'bsw', 'builder', 'bxt', 'byt', 'cfl', 'g33',
                      'g45', 'g965', 'glk', 'glk_iris', 'hsw', 'ilk', 'ivb',
                      'kbl', 'kbl_iris', 'skl', 'snb',
                      'gen9', 'gen9atom', 'gen9_iris', 'gen9atom_iris',
                      'bdw_iris', 'icl', 'icl_iris', 'tgl', 'dg2']


def parse_revisions(rev_table):
    # first row is the column headers
    # needed to discover build path if component build not available
    revisions = {}
    columns = []
    for column in rev_table[0].findall('td'):
        columns.append(column.attrib["value"])
    for rev in rev_table[1:]:
        index = 0
        project_details = {}
        for column in rev.findall('td'):
            project_details[columns[index]] = column.attrib["value"]
            index = index + 1
        (project, sha) = project_details["commit"].split("=")
        project_details["sha"] = sha
        if project == 'mesa_ci_internal' and not opts.internal:
            continue
        revisions[project] = project_details
    return revisions

def parse_components(comp_table):
    # needed to look up result path, or build type if component build not available
    components = []
    for component in comp_table:
        columns = component.findall('td')
        name = columns[0].attrib["value"]
        if 'href' in columns[0].attrib:
            url = columns[0].attrib["href"]
        else:
            # Component is incomplete, so skip it
            continue
        arch = columns[1].attrib["value"]
        hardware = columns[2].attrib["value"]
        build_type = columns[3].attrib["value"]
        config = columns[4].attrib["value"]
        duration = columns[5].attrib["value"]
        components.append({ "name" : name,
                            "url" : url,
                            "arch" : arch,
                            "hardware" : hardware, 
                            "type" : build_type,
                            "config" : config,
                            "duration" : duration})
    return components

def find_components(result_path, remote_result_path):
    components = {}
    rev_file = result_path + "/revisions.xml"
    if not os.path.exists(rev_file):
        # build was cleaned up
        return components
    revspec = RevisionSpecification.from_xml_file(rev_file)
    
    # populate the list of components by iterating build_info files in the output directory
    unwanted = ["test", "build_root"]
    for component in os.listdir(result_path):
        if component in unwanted:
            continue
    for root, dirs, files in os.walk(os.path.normpath(result_path)):
        for bad in unwanted:
            if bad in dirs:
                dirs.remove(bad)
        for afile in files:
            if "build_info" not in afile:
                continue
            base, ext = os.path.splitext(afile)
            o = Options(args=["build.py"])
            o.shard = base.split("_")[-1]
            dirs = root.split("/")
            o.hardware = dirs[-1]
            o.config = dirs[-2]
            o.arch = dirs[-3]
            project = dirs[-4]
            o.type = dirs[-5]
            o.result_path = remote_result_path
            p = ProjectInvoke(o, revision_spec=revspec, project=project)
            components[str(p)] = { "invoke" : p }

    known_files = {}
    for key, component in components.items():
        invoke = component["invoke"]
        test_files = invoke.get_info("test_files", block=False)
        if not test_files:
            continue
        for afile in test_files:
            known_files[os.path.basename(afile)] = key

    for root, adir, files in os.walk(os.path.normpath(result_path + "/test")):
        for afile in files:
            base, ext = os.path.splitext(afile)
            params = base.split("_")
            if params[0] == "flaky":
                params.pop(0)
                artifact_type = "flaky"
                project = params.pop(0)
                hardware = params.pop(0)
                arch = params.pop(0)
                shard = "0"
            elif ext == ".xml":
                artifact_type = "junit"
                project = params.pop(0)
                hardware = params.pop(0)
                arch = params.pop(0)
                shard = "0"
                if params:
                    shard = params.pop(0)
            elif ext == ".log":
                artifact_type = "log"
                project = params.pop(0)
                arch = params.pop(0)
                config = params.pop(0)
                assert(config == o.config)
                hardware = params.pop(0)
                shard = "0"
                if params:
                    shard = params.pop(0)
            else:
                continue

            if afile in known_files:
                component = components[known_files[afile]]
            else:
                # attempt to determine the source component from the
                # file name.  Legacy builds will not have the source
                # component stored in the build info metadata.
                
                # correct the project name from the xml filename,
                # which always begins with 'piglit-'
                if "crucible" in project:
                    project = "crucible-test"
                elif "glescts" in project:
                    project = "glescts-test"
                elif "glcts" in project:
                    project = "glcts-test"
                elif "vulkancts" in project:
                    project = "vulkancts-test"
                elif "deqp" in project:
                    project = "deqp-test"

                base_spec = next(iter(components.values()))["invoke"]
                base_spec = ProjectInvoke(from_string=str(base_spec))
                base_spec.project = project
                base_spec.options.shard = shard
                base_spec.options.hardware = hardware
                base_spec.options.arch = arch
                if str(base_spec) not in components:
                    print("WARN: could not locate component for file: " + afile)
                    continue
                component = components[str(base_spec)]
            if "artifacts" not in component:
                component["artifacts"] = []
            component["artifacts"].append({"type" : artifact_type,
                                           "file" : afile})
    return components

def convert_rsync_path(path):
    repl_path = "otc-mesa-ci.local::nfs/"
    if path.startswith("/mnt/jenkins/"):
        return path.replace("/mnt/jenkins/", repl_path)
    return path

def create_build_tar(url):
    project_map = { "mesa_master_daily" : "gl_main",
                    "vulkancts_daily" : "vulkan_main" }
    url_components = list(filter(None, url.split("/")))
    project = url_components[-2]

    if project in project_map:
        # fix jobs that do not have the same name as the project
        project = project_map[project]

    try:
        f = urlopen(url + "/api/python")
        build_page = ast.literal_eval(f.read().decode("utf-8"))
        f = urlopen(url + "/artifact/summary.xml")
        summary_table = et.fromstring(f.read())
    except:
        print("Could not export: " + url)
        return None

    tables = summary_table.findall('table')
    revisions = parse_revisions(tables[0].findall('tr'))
    components = parse_components(tables[1].findall('tr')[1:])

    # locate the output directory from the component parameters
    component = components[0]
    try:
        f = urlopen(component["url"] + "/api/python")
        component_page = ast.literal_eval(f.read().decode("utf-8"))
        params = component_page["actions"][0]["parameters"]
        for p in params:
            if p["name"] == "result_path":
                result_path = p["value"]
                break
        assert(result_path != None)
    except:
        # component is unavailable: construct the output directory from revisions table
        try:
            revspec = RevisionSpecification(revisions = revisions)
            hashstr = hashlib.md5(revspec.to_cmd_line_param().encode('UTF-8')).hexdigest()
        except:
            print("Could not export: " + url)
            return
            
        build_type = component["type"]
        results_dir = "/mnt/jenkins/results"
        result_path = "/".join([results_dir, project, hashstr, build_type])

    local_result_dir = tempfile.mkdtemp()
    local_result_path = os.path.join(local_result_dir, component["type"])
    run_batch_command(["rsync", "-r", "--exclude=build_root",
                       convert_rsync_path(result_path), local_result_dir],
                      quiet=True)
    components = find_components(local_result_path, result_path)
    if not components:
        print("Could not export: " + url)
        return

    # use the build info to get info about which builder the component
    # executed on, and timing details if available
    bad_components = []
    for key, component in components.items():
        invoke = component["invoke"]
        component["name"] = invoke.project
        component["shard"] = invoke.options.shard
        component["arch"] = invoke.options.arch
        component["hardware"] = invoke.options.hardware
        component["status"] = invoke.get_info("status", False)
        component["url"] = invoke.get_info("url", False)
        component["start_time"] = invoke.get_info("start_time", False)
        component["trigger_time"] = invoke.get_info("trigger_time", False)
        component["machine"] = invoke.get_info("machine", False)
        component["end_time"] = component["trigger_time"]
        end_time = invoke.get_info("end_time", False)
        if not component["url"]:
            bad_components.append(key)
            continue
        if end_time:
            component["end_time"] = end_time
        try:
            component["build"] = int(component["url"].split("/")[-2])
        except IndexError:
            bad_components.append(key)
            continue
        if (not component["status"] or 
            not component["url"] or
            not component["start_time"] or
            not component["trigger_time"] or
            (use_hardware_whitelist and
                not component["hardware"] in hardware_whitelist)):
            bad_components.append(key)
            continue
        try:
            f = urlopen(component["url"] + "/api/python")
            component_page = ast.literal_eval(f.read().decode("utf-8"))
            for action in component_page["actions"]:
                if "parameters" in action:
                    for p in action["parameters"]:
                        if p["name"] == "result_path":
                            if (invoke.options.result_path != p["value"]):
                                print("error: mismatched path:")
                                print("\t" + component["url"])
                                print("\t" + invoke.options.result_path)
                                print("\t" + p["value"])
                            break
                    break
        except:
            pass
        # can't be serialized into json
        del(component["invoke"])

    # remove components that couldn't be imported
    for key in bad_components:
        del(components[key])

    tmpdir = tempfile.mkdtemp()

    build_info = {
        "job" : url_components[-2],
        "build" : build_page["id"],
        "name" : build_page["displayName"],
        "start_time" : float(build_page["timestamp"]) / 1000,
        "end_time" : float(build_page["timestamp"] + build_page["duration"]) / 1000,
        "url" : url,
        "result_path" : result_path,
        "revisions" : revisions,
        "components" : [c for (_, c) in components.items()]
        }
    build_info_file = tmpdir + "/build_info.json"
    with open(build_info_file, "w") as fh:
        fh.write(json.dumps(build_info))

    comp_filename = "results_%s.%s.%s.tar" % (build_info["name"],
                                              build_info["job"],
                                              build_info["build"])
    comp_filename = comp_filename.replace("/", "_")
    comp_filepath = "%s/%s" % (tmpdir, comp_filename)

    with tarfile.open(comp_filepath, "w") as tf:
        os.chdir(tmpdir)
        tf.add("build_info.json")
        for root, adir, files in os.walk(os.path.normpath(local_result_path + "/test")):
            os.chdir(root)
            for afile in files:
                tf.add(afile)
    # limit threads for compression so master doesn't get overwhelmed
    threads = multiprocessing.cpu_count()
    if threads > 16:
        threads = 16
    cmd = ["xz", "-T" + str(threads), comp_filepath]

    ret = subprocess.call(cmd)
    comp_filepath += ".xz"
    if ret:
        raise RuntimeError("Compression with xz failed!")

    print("Results successfully compressed to: %s" % comp_filepath)

    shutil.rmtree(local_result_dir)

    return comp_filepath


def upload_tar(tar_file):
    scp_cmd = ['scp']
    scp_host = ""
    # Disable host key verification
    scp_cmd.extend(['-o', 'StrictHostKeyChecking=no'])
    scp_cmd.extend(['-o', 'UserKnownHostsFile=/dev/null'])
    user = pwd.getpwuid(os.getuid()).pw_name
    if "MESA_CI_RESULT_HOST_PORT" in os.environ:
        scp_port = os.environ["MESA_CI_RESULT_HOST_PORT"]
        scp_cmd.extend(['-P', scp_port])
    scp_keyfile = None
    if "MESA_CI_SSH_KEYFILE" in os.environ:
        scp_keyfile = os.path.expanduser(os.environ["MESA_CI_SSH_KEYFILE"])
        scp_cmd.extend(['-i', scp_keyfile])
    scp_dest = '/tmp/mesa_ci_results'
    if "MESA_CI_RESULT_HOST_PATH" in os.environ:
        scp_dest = os.environ["MESA_CI_RESULT_HOST_PATH"]
    if "MESA_CI_RESULT_USER" in os.environ:
        user = os.environ["MESA_CI_RESULT_USER"]
        scp_host = user + "@"
    host = 'localhost'
    if "MESA_CI_RESULT_HOST" in os.environ:
        host = os.environ["MESA_CI_RESULT_HOST"]
    scp_cmd.append(tar_file)
    scp_host += host
    scp_dest = '%s:%s' % (scp_host, scp_dest)
    scp_cmd.append(scp_dest)
    subprocess.call(scp_cmd)


def export_all_builds(url, limit=None):
    f = urlopen(url + "/api/python")
    job_page = ast.literal_eval(f.read().decode("utf-8"))
    builds = job_page["builds"]
    if limit:
        builds.sort(key=lambda x: x['number'])
        builds = builds[-limit:]
    for build in builds:
        tar_file = create_build_tar(build["url"])
        if not tar_file:
            continue
        upload_tar(tar_file)
        shutil.rmtree(os.path.normpath(tar_file + "/.."))

parser = argparse.ArgumentParser(description=("Exports test data to a remote"
                                              " system for importing into SQL"))
parser.add_argument('--url', type=str, required=True,
                    help="Jenkins url for the build (eg, " +
                    "http://otc-mesa-ci.jf.intel.com/job/mesa_master/13643/")

parser.add_argument('--internal', action='store_true',
                    help=('Include hardware that is not in the hardware '
                          'whitelist'))
parser.add_argument('--limit', type=int,
                    help=("If exporting from a top-level job URL (and not "
                          "an individual build URL), limit the number "
                          "of builds exported to the latest LIMIT amount. "
                          "Must be > 0 if specified. Default: all (as many as "
                          "it can grab from jenkins)"))
opts = parser.parse_args()
if opts.limit is not None and opts.limit < 1:
    parser.print_help()
    sys.exit(1)
build_id = None
url_components = list(filter(None, opts.url.split("/")))
use_hardware_whitelist = not opts.internal
if len(url_components) > 4:
    build_id = url_components[-1]
if build_id:
    tar_file = create_build_tar(opts.url)
    if not tar_file:
        sys.exit(-1)
    upload_tar(tar_file)
    shutil.rmtree(os.path.normpath(tar_file + "/.."))
    sys.exit(0)

# else we have a url to a job, and we want to iterate the builds
export_all_builds(opts.url, limit=opts.limit)
